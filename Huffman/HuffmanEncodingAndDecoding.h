#ifndef _HEADFIC_HUFFMAN_ENCODING_AND_DECODING
#define _HEADFIC_HUFFMAN_ENCODING_AND_DECODING

#include "global.h"

#define _is1Bit(bit) (bit)
#define _isNOTZeroWeigth(Lea) (Lea)
#define _isStructNotDone (!isFirstNodeEqualLastNode())
#define _isStrCopyDone(str) (str)

#define _isLeaves(node) (((node)->LeftNode == NULL) && ((node)->RighNode == NULL))

#define MAX_BINARY_TREE_SUBNODE 2

#define MASK_1BIT 0x01
#define MASK_2BIT 0x02
#define MASK_3BIT 0x04
#define MASK_4BIT 0x08
#define MASK_5BIT 0x10
#define MASK_6BIT 0x20
#define MASK_7BIT 0x40
#define MASK_8BIT 0x80

/* -- Encode -- */
void structLeaves(Histogram hist[], uint16_t Len);
Node *structHuffmanTree();

Node *structBranch(Node *Bunarch, Node *Leaves[]);
void findTwoMinLeaves(Node *node[]);
Node *findMinNodeAndRemoveLink(Node *node);
Node *getMinNode();

void freeHuffmanTree(Node *Treetop);
void VisitHuffmanTree(Node *Treetop, Histogram hist[], char huffCod[], uint8_t *index);
void StrCopy(Histogram hist[], char str[], Node *tmp);

/* -- Decode -- */
uint8_t getBitOfByte(uint8_t Byte, uint8_t bit);

#endif
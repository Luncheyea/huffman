﻿
#include "global.h"


int main(void) {
	char FilePatch[MAX_FILE_PATH] = { '\0' };
	uint8_t isEencode = 0;
Cycle:
	memset(FilePatch, '\0', MAX_FILE_PATH);

	fprintf(stdout, "Huffman : Eencode(1) or Decode(0) ? ");
	fflush(stdin);
	fscanf(stdin, "%hhu", &isEencode);

	if (isEencode) {
		fprintf(stdout, "\nHuffman Encode Image Path :\n");
		fflush(stdin);
		fscanf(stdin, " %[^\n]s", FilePatch);
		Encoding(FilePatch);
	}
	else {
		fprintf(stdout, "\nHuffman Decode Image Path :\n");
		fflush(stdin);
		fscanf(stdin, " %[^\n]s", FilePatch);
		Decode(FilePatch);
	}
	fprintf(stdout, "===========================================\n\n");

	goto Cycle;
	return 0;
}
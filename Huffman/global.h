#ifndef _HEADFIC_GLOBAL
#define _HEADFIC_GLOBAL

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


typedef struct Double_LinkList {
	struct Double_LinkList *PreNode;
	struct Double_LinkList *NexNode;
	uint32_t Data;
	uint8_t Label;
	struct Double_LinkList *LeftNode;
	struct Double_LinkList *RighNode;
} Node;

typedef struct Pixel_Information {
	uint8_t Label;
	uint32_t Weigth;
	uint8_t HufCod[32];
} Histogram;

#define MAX_FILE_PATH 0x00000100u


/* -- funct -- */
void Encoding(char FILE_ADDRESS[]);
void Decode(char FILE_HUFFMAN[]);

/* -- Huffman Encoding And Decoding -- */
void HuffmanEncoding(Histogram hist[], uint16_t Len);
void HuffmanDecoding(Histogram hist[], uint16_t Len, FILE *file, char FILE_NAME[]);

/* -- Link List -- */
Node *createNode();
Node *RemoveLinkOfNode(Node *node);
Node *removeNode(Node *node);
Node *getFirstNode();
Node *getLastNode();
uint8_t isFirstNodeEqualLastNode();

#endif
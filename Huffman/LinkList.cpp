
#include "global.h"
#include "LinkList.h"

Node *newNode = NULL, *firstNode = NULL, *lastNode = NULL, 
	 *secondlastNode = NULL;

Node *createNode() {
	newNode = (Node *)calloc(1, sizeof(Node));

	if (_isLinKList)
		linkTolastNode(newNode);
	else
		initializationLinkList(newNode);

	return newNode;
}

void linkTolastNode(Node *node) {
	lastNode = node;
	_LinkTheLastTwoNode;
	secondlastNode = node;
}

void initializationLinkList(Node *node) {
	firstNode = node;
	lastNode = node;
	secondlastNode = node;
}

Node *RemoveLinkOfNode(Node *node) {
	if (_isFirstNode(node))
		node = removeFirstNodeLink(node);
	else if (_isLastNode(node))
		node = removeLastNodeLink(node);
	else
		_LinkThePreviousAndNextNode(node);

	return node;
}

Node *removeNode(Node *node) {
	node = RemoveLinkOfNode(node);
	free(node);

	return node;
}

Node *removeFirstNodeLink(Node *node) {
	_NodePointerMoveBackward(firstNode);
	if (_isNOTLastNode(node))
		_ClearLinkBetweenNextAndThisNode(node);

	return node;
}

Node *removeLastNodeLink(Node *node) {
	_NodePointerMoveForward(lastNode);
	_NodePointerMoveForward(secondlastNode);
	if (_isNOTFirstNode(node))
		_ClearLinkBetweenPrevAndThisNode(node);

	return node;
}

Node *getFirstNode() {
	return firstNode;
}

Node *getLastNode() {
	return lastNode;
}

uint8_t isFirstNodeEqualLastNode() {
	return (firstNode == lastNode) ? 1 : 0;
}
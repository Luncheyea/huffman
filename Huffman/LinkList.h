#ifndef _HEADFIC_LINK_LIST
#define _HEADFIC_LINK_LIST

#include "global.h"


#define _isLinKList (firstNode != NULL)

#define _isFirstNode(node) ((node)->PreNode == NULL)
#define _isLastNode(node) ((node)->NexNode == NULL)

#define _isNOTFirstNode(node) ((node)->PreNode != NULL)
#define _isNOTLastNode(node) ((node)->NexNode != NULL)

#define _NodePointerMoveForward(pointer) ((pointer) = node->PreNode)
#define _NodePointerMoveBackward(pointer) ((pointer) = node->NexNode)

#define _ClearLinkBetweenPrevAndThisNode(node) ((node)->PreNode->NexNode = NULL)
#define _ClearLinkBetweenNextAndThisNode(node) ((node)->NexNode->PreNode = NULL)

#define _LinkThePreviousAndNextNode(node) { \
	(node)->PreNode->NexNode = (node)->NexNode; \
	(node)->NexNode->PreNode = (node)->PreNode; \
}

#define _LinkTheLastTwoNode { \
	secondlastNode->NexNode = lastNode; \
	lastNode->PreNode = secondlastNode; \
}


void linkTolastNode(Node *node);
void initializationLinkList(Node *node);
Node *removeFirstNodeLink(Node *node);
Node *removeLastNodeLink(Node *node);

#endif
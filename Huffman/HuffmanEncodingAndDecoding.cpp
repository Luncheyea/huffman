
#include "global.h"
#include "HuffmanEncodingAndDecoding.h"


void HuffmanEncoding(Histogram hist[], uint16_t Len) {
	char huffCod[32] = { '\0' };
	uint8_t index = 0;

	structLeaves(hist, Len);
	Node *TreeTop = structHuffmanTree();
	VisitHuffmanTree(TreeTop, hist, huffCod, &index);
	freeHuffmanTree(TreeTop);
}

void structLeaves(Histogram hist[], uint16_t Len) {
	Node *Leaves;
	uint16_t i;

	for (i = 0; i < Len; i++)
		if (_isNOTZeroWeigth(hist[i].Weigth)) {
			Leaves = createNode();
			Leaves->Data = hist[i].Weigth;
			Leaves->Label = hist[i].Label;
		}
}

Node *structHuffmanTree() {
	Node *Leaves[MAX_BINARY_TREE_SUBNODE], *Branches = NULL;

	while (_isStructNotDone) {
		findTwoMinLeaves(Leaves);

		Branches = createNode();
		Branches = structBranch(Branches, Leaves);
	}

	return Branches;
}

void findTwoMinLeaves(Node *node[]) {
	uint8_t i;
	for (i = 0; i < MAX_BINARY_TREE_SUBNODE; i++)
		node[i] = findMinNodeAndRemoveLink(node[i]);
}

Node *findMinNodeAndRemoveLink(Node *node) {
	node = getMinNode();
	node = RemoveLinkOfNode(node);

	return node;
}

Node *getMinNode() {
	Node *tmp, *min;

	tmp = getFirstNode();
	min = tmp;
	while (tmp != NULL) {
		if (min->Data > tmp->Data)
			min = tmp;
		tmp = tmp->NexNode;
	}

	return min;
}

Node *structBranch(Node *Bunarch, Node *Leaves[]) {
	Bunarch->LeftNode = Leaves[0];
	Bunarch->RighNode = Leaves[1];
	Bunarch->Data = Leaves[0]->Data + Leaves[1]->Data;

	return Bunarch;
}

void freeHuffmanTree(Node *Treetop) {
	if (Treetop->LeftNode != NULL) {
		freeHuffmanTree(Treetop->LeftNode);
		freeHuffmanTree(Treetop->RighNode);

		removeNode(Treetop);
	}
	else
		removeNode(Treetop);
}

void VisitHuffmanTree(Node *Treetop, Histogram hist[], char huffCod[], uint8_t *index) {
	if (Treetop->LeftNode != NULL) {
		huffCod[(*index)] = '1';
		VisitHuffmanTree(Treetop->LeftNode, hist, huffCod, &(++(*index)));

		// The left side is 1; The right side is 0.

		huffCod[(*index)] = '0';
		VisitHuffmanTree(Treetop->RighNode, hist, huffCod, &(++(*index)));
		(*index)--;
	}
	else {
		huffCod[(*index)] = '\0';
		(*index)--;

		StrCopy(hist, huffCod, Treetop);
	}
}

void StrCopy(Histogram hist[], char str[], Node *tmp) {
	uint8_t i = 0;

	while (_isStrCopyDone(str[i])) {
		hist[tmp->Label].HufCod[i] = str[i];
		i++;
	}
	hist[tmp->Label].HufCod[i] = '\0';
}

void HuffmanDecoding(Histogram hist[], uint16_t Len, FILE *file, char FILE_NAME[]) {
	structLeaves(hist, Len);
	Node *TreeTop = structHuffmanTree();

	FILE *decHufFile = fopen(FILE_NAME, "wb");

	Node *decNode = TreeTop;
	uint8_t HufCodByte, HufCodBit;
	int8_t index;
	while ((fread(&HufCodByte, sizeof(uint8_t), 1, file)) != NULL) {
		for (index = 7; index >= 0; index--) {
			HufCodBit = getBitOfByte(HufCodByte, index + 1);

			if (_is1Bit(HufCodBit))
				decNode = decNode->LeftNode;
			else
				decNode = decNode->RighNode;

			if (_isLeaves(decNode)) {
				fprintf(decHufFile, "%c", decNode->Label);
				decNode = TreeTop;
			}
		}
	}
	fclose(decHufFile);
	freeHuffmanTree(TreeTop);
}

uint8_t getBitOfByte(uint8_t Byte, uint8_t bit) {
	switch (bit) {
	case 1:
		return Byte & MASK_1BIT;
	case 2:
		return Byte & MASK_2BIT;
	case 3:
		return Byte & MASK_3BIT;
	case 4:
		return Byte & MASK_4BIT;
	case 5:
		return Byte & MASK_5BIT;
	case 6:
		return Byte & MASK_6BIT;
	case 7:
		return Byte & MASK_7BIT;
	case 8:
		return Byte & MASK_8BIT;
	}

	return -1;
}
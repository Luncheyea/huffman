#include "global.h"
#include "funct.h"


void Encoding(char FILE_ADDRESS[]) {
	Histogram hist[BIT8_GRAYSCALE] = { NULL };

	FILE *file;
	if ((file = fopen(FILE_ADDRESS, "rb")) == NULL) _return_EncodeERROR;

	uint8_t Value;
	uint32_t originalFileLength = 0;
	while ((fscanf(file, "%c", &Value) != EOF)) {
		_countEveryByteWeight;
		_countOriginalFileLength;
	}

	fclose(file);
	uint16_t i;
	for (i = 0; i < BIT8_GRAYSCALE; i++)
		_MarkWeightCorrespondToByte;

	char exName[4] = { '\0' };
	getFilenameExtension(FILE_ADDRESS, exName);
	changeFilenameExtension(FILE_ADDRESS, HUFFMAN_EXNAME);
	HuffmanEncoding(hist, BIT8_GRAYSCALE);

	uint32_t HuffmanFileLength = 0;
	HuffmanFileLength = saveEncode(hist, BIT8_GRAYSCALE, FILE_ADDRESS, exName);

	printFileLengthAndCompressionRate(originalFileLength, HuffmanFileLength);
}

void printFileLengthAndCompressionRate(uint32_t oriFileLen, uint32_t HufFileLen) {
	fprintf(stdout, "-----------\n");
	fprintf(stdout, "Original File Length : %u Bytes\n", oriFileLen);
	fprintf(stdout, "Huffman  File Length : %u Bytes\n", HufFileLen);
	fprintf(stdout, "Compression Rate : %.3lf%%\n", ((double)HufFileLen / oriFileLen) * 100);
}

void changeFilenameExtension(char Filename[], char exName[]) {
	uint8_t Length = 0, index;
	_getTheFilenameLength(Filename);
	_ChangeTheExFilename(Filename);
}

void getFilenameExtension(char Filename[], char exName[]) {
	uint8_t Length = 0, index;
	_getTheFilenameLength(Filename);
	_getTheExFilename(Filename);
}

uint32_t saveEncode(Histogram hist[], uint32_t Len, char IMAGE_HUFFMAN[], char exName[]) {
	FILE *encFileHuf;
	uint32_t hufcodLength = 0;
	uint16_t i;

	encFileHuf = fopen(IMAGE_HUFFMAN, "wb");
	if (_isTheFileExist(encFileHuf)) {

		_saveFileCheckCodeAndExFileName;
		_saveByteWeight;
		saveEncToTmp(hist, IMAGE_HUFFMAN, exName);
		hufcodLength = saveEncByteToFile(encFileHuf);

		fclose(encFileHuf);
		fprintf(stdout, ENCODE_SUCCESS);
	}
	else
		fprintf(stdout, ENCODE_FAILED);
	return hufcodLength; //+ FILE_HEADER_LENGTH; 
						 //not count header length
}

void saveEncToTmp(Histogram hist[], char IMAGE_HUFFMAN[], char exName[]) {
	FILE *file, *tmp;

	changeFilenameExtension(IMAGE_HUFFMAN, exName);
	if ((file = fopen(IMAGE_HUFFMAN, "rb")) == NULL) _return_EncodeERROR;
	if ((tmp = fopen(HUFFMAN_TMP_PATH, "wb")) == NULL) _return_EncodeERROR;

	uint8_t Value;
	while ((fscanf(file, "%c", &Value) != EOF))
		_saveDecode(tmp);

	fclose(tmp);
	fclose(file);
}

uint32_t saveEncByteToFile(FILE *encFileHuf) {
	uint8_t Binary[8] = { '0' }, Decimal;
	uint32_t hufcodLength = 0;
	FILE *tmp;
	if ((tmp = fopen(HUFFMAN_TMP_PATH, "rb")) == NULL) _return0_EncodeERROR;

	while ((fread(Binary, sizeof(uint8_t), 8, tmp)) != NULL) {
		Decimal = BinToDec_8BIT(Binary);
		fprintf(encFileHuf, "%c", Decimal);
		memset(Binary, '0', 8);
		_countHuffmanCodeLength;
	}

	fclose(tmp);
	remove(HUFFMAN_TMP_PATH);
	return hufcodLength;
}

uint8_t BinToDec_8BIT(uint8_t *Binary) {
	uint8_t index, Weight = 128, Decimal = 0;
	for (index = 0; index < 8; index++) {
		Decimal += Weight * (Binary[index] - 48);
		Weight /= 2;
	}

	return Decimal;
}

void Decode(char FILE_HUFFMAN[]) {
	FILE *decFileHuf = fopen(FILE_HUFFMAN, "rb");
	if (_isTheFileNOTExist(decFileHuf)) _return_DecodeERROR;

	uint32_t check;
	char exName[4] = { '\0' };
	_getFileCheckCodeAndExFileName;
	_INC_FilePointer(decFileHuf);
	_INC_FilePointer(decFileHuf);
	if (_isNOTCheckCode(check)) _return_DecodeERROR;


	Histogram hist[BIT8_GRAYSCALE] = { 0 };
	uint16_t i;
	for (i = 0; i < BIT8_GRAYSCALE; i++) {
		_MarkWeightCorrespondToByte;
		_getByteWeight;
	}

	changeFilenameExtension(FILE_HUFFMAN, exName);
	HuffmanDecoding(hist, BIT8_GRAYSCALE, decFileHuf, FILE_HUFFMAN);

	fclose(decFileHuf);
	fprintf(stdout, DECODE_SUCCESS);
}
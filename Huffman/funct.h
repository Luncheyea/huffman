#ifndef _HEADFIC_FUNCT
#define _HEADFIC_FUNCT

#include "global.h"


#define MAX_FILENAME_EXTENSION 0x00000003u
#define BIT8_GRAYSCALE		   0x00000100u
#define CHECK_CODE			   0x3A417020u
#define FILE_HEADER_LENGTH	   0x0000040Eu

#define ENCODE_SUCCESS "\nThe Huffman Encoding is completed.\n"
#define DECODE_SUCCESS "\nThe Huffman Decoding is completed.\n"

#define ENCODE_FAILED "\nThe Huffman Encoding is failed.\n"
#define DECODE_FAILED "\nThe Huffman Decoding is failed.\n"

#define HUFFMAN_TMP_PATH "huffman.tmp"
#define HUFFMAN_EXNAME "huf"

#define _return_EncodeERROR { \
	fprintf(stdout, ENCODE_FAILED); \
	return; \
}
#define _return0_EncodeERROR { \
	fprintf(stdout, ENCODE_FAILED); \
	return 0; \
}
#define _countEveryByteWeight hist[Value].Weigth++
#define _countOriginalFileLength originalFileLength++
#define _countHuffmanCodeLength hufcodLength++
#define _MarkWeightCorrespondToByte hist[i].Label = (uint8_t)i

#define _isTheFileExist(file) ((file) != NULL)
#define _isTheFileNOTExist(file) ((file) == NULL)
#define _saveFileCheckCodeAndExFileName fprintf(encFileHuf, "%X %s\n\r", CHECK_CODE, exName)
#define _saveByteWeight { \
	for (i = 0; i < Len; i++) \
		fwrite(&hist[i].Weigth, sizeof(uint32_t), 1, encFileHuf); \
}
#define _saveDecode(fptr) { \
	fprintf((fptr), "%s", hist[Value].HufCod); \
}

#define _getTheFilenameLength(filename) {while (filename[Length] != '\0') Length++;}
#define _ChangeTheExFilename(filename) { \
	for (index = 0; index < MAX_FILENAME_EXTENSION; index++) \
		(filename)[Length - MAX_FILENAME_EXTENSION + index] = exName[index]; \
}
#define _getTheExFilename(filename) {\
	for (index = 0; index < MAX_FILENAME_EXTENSION; index++) \
		exName[index] = (filename)[Length - MAX_FILENAME_EXTENSION + index]; \
}

#define _isNOTCheckCode(Code) ((Code) != CHECK_CODE)
#define _getFileCheckCodeAndExFileName fscanf(decFileHuf, " %X %s", &check, exName)
#define _getByteWeight fread(&hist[i].Weigth, sizeof(uint32_t), 1, decFileHuf)
#define _INC_FilePointer(ptr) fgetc(ptr)
#define _return_DecodeERROR { \
	fprintf(stdout, DECODE_FAILED); \
	return; \
}

void printFileLengthAndCompressionRate(uint32_t oriFileLen, uint32_t HufFileLen);
void getFilenameExtension(char Filename[], char exName[]);
void changeFilenameExtension(char Filename[], char exName[]);
uint32_t saveEncode(Histogram hist[], uint32_t Len, char IMAGE_HUFFMAN[], char exName[]);
void saveEncToTmp(Histogram hist[], char IMAGE_HUFFMAN[], char exName[]);
uint32_t saveEncByteToFile(FILE *encFileHuf);
uint8_t BinToDec_8BIT(uint8_t *Binary);

#endif